# A Small Task

## Introduction

In this project you will find a small task that is provided to applicants at Hotellistat.
It enables you to prove yourself by finding a good solution to the given problem,
abiding by best practices that you normally use in your projects.
It also enables us to judge your proficiency in certain areas that we require you to work in.

## Tooling

### Setup

To develop in this project you will first need to install all required dependencies.
You will need to install node.js and a packacke manager like `yarn` or `npm` to execute
commands in js.

To install all required dependencies run:

```
$ yarn

  # Or

$ npm install
```

### Json Server

We have provided a simple node project which contains a `serve` script. The `serve` script
will provide you with a simple json-server which you can use to access data
for your frontend task.

you can launch the server by typing:

```
$ yarn serve

  # Or

$ npm run serve
```

### Frontend application

Provided in the repository you will find a prebuilt application in the `src` folder.
It contains a very simple Vue.js v3 project which is bundled and served by Vite.js

To launch the dev server type:

```
$ yarn dev

  # Or

$ npm run dev
```

# The task

We would like you to create a list view of all **active** user accounts that are provided under the `http://localhost:3000/user_accounts` endpoint of the json-server.
The list should contain list items, which display the Full name and the email of each user.

In addition to that, we want to show a round avatar placeholder (`/src/assets/avatar.jpg`) of the user on the left side next to the Full name and email.

When you click on one of the listings, a modal should pop up, which displays all available data of the respective user.
The Full name (name_first and name_last) and the email should be editable through input fields.

When updating one or more of these three fields, a `PATCH` request should be sent to the json-server upon form submission,
to update the respective field/s.

Please refer to the [json-server documentation](https://github.com/typicode/json-server) for further information on the usage of the json-server.

> Note: We prefer to use TailwindCSS for all of our products, thus we would like for you to try and solve the task by using TailwindCSS if possible. Normal CSS is also allowed, but keep in mind that the CSS needs to be defined **within** the Vue components and must not be a global .css file.
